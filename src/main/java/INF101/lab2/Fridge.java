package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    int max_size = 20;
    ArrayList<FridgeItem> fridgeItems;

    public Fridge() {
        fridgeItems = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        /**
         * Returns the number of items currently in the fridge
         *
         * @return number of items in the fridge
         */
        return fridgeItems.size();
    }

    @Override
    public int totalSize() {
        /**
         * The fridge has a fixed (final) max size.
         * Returns the total number of items there is space for in the fridge
         *
         * @return total size of the fridge
         */
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        /**
         * Place a food item in the fridge. Items can only be placed in the fridge if
         * there is space
         *
         * @param item to be placed
         * @return true if the item was placed in the fridge, false if not
         */
        if (nItemsInFridge() < max_size) {
            fridgeItems.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        /**
         * Remove item from fridge
         *
         * @param item to be removed
         * @throws NoSuchElementException if fridge does not contain <code>item</code>
         */
        if (!fridgeItems.contains(item)) {
            throw new NoSuchElementException();
        }
        fridgeItems.remove(item);

    }

    @Override
    public void emptyFridge() {
        /**
         * Remove all items from the fridge
         */
        fridgeItems.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        /**
         * Remove all items that have expired from the fridge
         * @return a list of all expired items
         */
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem item : fridgeItems) {
            if (item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        fridgeItems.removeAll(expiredFood);
        return expiredFood;
    }
}
